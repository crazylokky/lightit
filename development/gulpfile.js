'use strict';

const gulp = require('gulp');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const cleanCss = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const gcmq = require('gulp-group-css-media-queries');
const stylelintGulp = require('gulp-stylelint');
const notify = require('gulp-notify');

function swallowError(error) {
  console.log(error.toString());
  this.emit('end');
}

gulp.task('AppScss', function() {

  return gulp.src(['./sass/**/*.scss'])
    .pipe(sass().on('error', swallowError))
    .pipe(autoprefixer(['last 5 versions', 'safari 5', 'ie 10', 'opera 12.1', 'ios 6', 'android 4'], {cascade: true}))
    .pipe(gcmq())
    .pipe(cleanCss())
    .pipe(gulp.dest('../src/'));
});

gulp.task('watch', function() {
  gulp.watch(['./sass/*.scss', './sass/**/*.scss'], gulp.series('AppScss'));
});

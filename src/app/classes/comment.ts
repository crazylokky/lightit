import {User} from './user';

export class Comment {
  id: number;
  pid?: number;
  created_by?: User;
  rate: number;
  text: string;
}

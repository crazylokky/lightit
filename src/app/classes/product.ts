import { Comment } from './comment';

export class Product {
  id: number;
  img?: string;
  text?: string;
  title: string;
  comments?: Comment[];
}

import {Component, OnInit, Input} from '@angular/core';
import {User} from '../classes/user';
import {SmkTestingApiService} from '../smk-testing-api.service';
import {Router} from '@angular/router';
import {Token} from '../classes/token';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['../../styles/form/login.css'],
  providers: [SmkTestingApiService]
})
export class LoginComponent implements OnInit {
  @Input() formUrl: string;
  submitText = 'Login';
  class = 'formLogin';
  user: User = {
    // login: testUser
    // password: qwerty123
  };
  errors = [];

  constructor(private router: Router, private smkTestingApiService: SmkTestingApiService) {
    if (this.router.url === '/logout') {
      localStorage.removeItem('token');
    }
  }

  ngOnInit() {
  }

  submitForm() {
    this.errors = [];
    if (!this.user.username) {
      this.errors.push('Sorry, login is required');
    }
    if (!this.user.password) {
      this.errors.push('Sorry, password is required');
    }
    if (this.errors.length === 0) {
      this.smkTestingApiService.getToken(this.user, this.formUrl).subscribe((response: Token) => {
        if (response.hasOwnProperty('token')) {
          localStorage.setItem('token', response.token);
          this.router.navigate(['products']);
        } else {
          if (response.hasOwnProperty('message')) {
            this.errors.push(response.message);
          } else {
            this.errors.push('Sorry, something went wrong :(');
          }
        }
      });
    }
    return;
  }
}

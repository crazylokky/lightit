import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(public router: Router) {}

  private isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    return token !== null;
  }

  canActivate(): boolean {
    if (!this.isAuthenticated()) {
      this.router.navigate(['preview']);
      return false;
    }
    return true;
  }
}

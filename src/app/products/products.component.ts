import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Product} from '../classes/product';
import {Comment} from '../classes/comment';
import {SmkTestingApiService} from '../smk-testing-api.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['../../styles/products.css'],
  providers: [SmkTestingApiService]
})
export class ProductsComponent implements OnInit {

  errors = [];
  preview: boolean;
  loginText = 'Login';
  logoutText = 'Logout';
  productActive: Product;
  productsList: Product[];
  sendCommentText = 'Send Comment';
  userComment: Comment = new Comment();
  imageApiUrl = 'http://smktesting.herokuapp.com/static/';

  constructor(private router: Router, private smkTestingApiService: SmkTestingApiService) {
    this.preview = this.router.url === '/preview';
    if (!this.preview) {
      this.userComment.rate = 1;
    }
    this.smkTestingApiService.getProducts().subscribe((products: Product[]) => {
      this.productsList = products;
      if (this.productsList.length > 0) {
        for (let i = 0; i < this.productsList.length; i++) {
          this.smkTestingApiService.getProductComments(this.productsList[i].id).subscribe((comments: Comment[]) => {
            this.productsList[i].comments = comments;
            this.productActive = this.productsList[i];
          });
        }
      }
    });
  }

  ngOnInit() {
  }

  range(index: number) {
    return Array(index).fill(null).map((x, i) => i);
  }

  setActiveProduct(product: Product) {
    this.productActive = product;
  }

  submitCommentsForm(stars: number, text: string) {
    this.errors = [];
    if (!text) {
      this.errors.push('Sorry, comment text is required');
    }
    if (this.errors.length === 0) {
      this.userComment.id = this.productActive.id;
      this.smkTestingApiService.sendProductComment(this.userComment).subscribe((response: any) => {
        if (response.hasOwnProperty('review_id')) {
          this.smkTestingApiService.getProductComments(this.productActive.id).subscribe((comments: Comment[]) => {
            this.productActive.comments = comments;
          });
        } else {
          this.errors.push('Sorry, something went wrong :(');
        }
      });
    }
    return;
  }

  starSelectionChange(rate: number) {
    this.userComment.rate = ++rate;
  }

}

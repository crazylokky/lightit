import { TestBed, inject } from '@angular/core/testing';

import { SmkTestingApiService } from './smk-testing-api.service';

describe('SmkTestingApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SmkTestingApiService]
    });
  });

  it('should be created', inject([SmkTestingApiService], (service: SmkTestingApiService) => {
    expect(service).toBeTruthy();
  }));
});

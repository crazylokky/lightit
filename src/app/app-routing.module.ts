import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductsComponent } from './products/products.component';
import { InitFormComponent } from './init-form/init-form.component';
import { AuthGuardService as AuthGuard} from './auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: '/products', pathMatch: 'full' },
  { path: 'login', component: InitFormComponent },
  { path: 'logout', component: InitFormComponent },
  { path: 'products', component: ProductsComponent, canActivate: [AuthGuard] },
  { path: 'preview', component: ProductsComponent}
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ],
  declarations: [],
  providers: [AuthGuard]
})
export class AppRoutingModule { }

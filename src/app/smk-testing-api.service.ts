import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';

import {User} from './classes/user';
import {Comment} from './classes/comment';

import {HttpHeaders} from '@angular/common/http';

@Injectable()
export class SmkTestingApiService {

  private baseUrl = 'http://smktesting.herokuapp.com/api/';
  httpOptions: {};

  constructor(private http: HttpClient) {
    const token = localStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': token
      })
    };
  }

  getToken(user: User, apiUrl: string) {
    const url = this.baseUrl + apiUrl + '/';
    return this.http.post(url, {
      username: user.username,
      password: user.password
    }).pipe(
      catchError(this.handleError('getToken'))
    );
  }

  getProducts() {
    const url = this.baseUrl + 'products/';
    return this.http.get(url, this.httpOptions).pipe(
      catchError(this.handleError('getProducts'))
    );
  }

  getProductComments(id: number) {
    const url = this.baseUrl + 'reviews/' + id;
    return this.http.get(url, this.httpOptions).pipe(
      catchError(this.handleError('getComments'))
    );
  }

  sendProductComment(comment: Comment) {
    // Get API error: "Review.created_by" must be a "User" instance
    const url = this.baseUrl + 'reviews/' + comment.id;
    return this.http.post(url, {
      rate: comment.rate,
      text: comment.text,
    }, this.httpOptions).pipe(
      catchError(this.handleError('getComments'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}

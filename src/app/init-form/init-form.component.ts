import {Tab} from '../classes/tab';
import {Component, OnInit, ElementRef} from '@angular/core';
import {LoginComponent} from '../login/login.component';

@Component({
  selector: 'app-init-form',
  templateUrl: './init-form.component.html',
  styleUrls: ['../../styles/init-form.css']
})
export class InitFormComponent implements OnInit {
  tabs: Tab[] = [
    {label: 'Login', data: 'login'},
    {label: 'Registration', data: 'register'}
  ];

  tabActive: Tab = this.tabs[0];

  constructor() {
  }

  ngOnInit() {
  }

  setActiveTab(tab: Tab) {
    this.tabActive = tab;
  }
}
